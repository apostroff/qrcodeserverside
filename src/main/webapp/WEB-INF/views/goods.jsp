<%@ page language="java" contentType="text/html; charset=utf8"
	pageEncoding="utf8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf8">
	<title><spring:message code="label.name"/></title>
	
</head>
<body>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.13/jquery-ui.min.js"></script>
  
<h2>Goods </h2>

<form:form method="post" action="list/add" commandName="goods">

	<table>
		<tr>
			<td><form:label path="name">
				Name
			</form:label></td>
			<td><form:input path="name" /></td>
		</tr>
		<tr>
			<td><form:label path="desc">
				Desc
			</form:label></td>
			<td><form:input path="desc" /></td>
		</tr>
		<tr>
			<td><form:label path="price">
				Price
			</form:label></td>
			<td><form:input path="price" /></td>
		</tr>
		<tr>
			<td><form:label path="url">
				URL
			</form:label></td>
			<td><form:input path="url" /></td>
		</tr>
		<tr>
			<td colspan="2"><input type="submit"
				value="ADD" /></td>
		</tr>
	</table>
</form:form>`
<h4> test request</h4>
<button id="testbutton" onclick="onClickTestButton();return false"> send test request</button>

<form action="goodscreate" method="post">
<input id="jn" name="jn" type="text" value="jopa negra">
<input type="submit" value="test"> 
</form>
<script type="text/javascript">
 	function onClickTestButton(){
 		console.debug("onClickTestButton: " );
 		var newGoods = {
 				name:"tetsname" , desc:"testdesc" , price:5.7 , url:"FIGNYZ"  
 		};
 		
 		$.postJSON("goodscreate" , newGoods ,  function(
 			      idProductCategory)
 			     {
 			       console.debug("Inserted: " + idProductCategory);
 			     });
 			  
 	}
 	$.postJSON = function(url, data, callback) {
 		console.debug("ajax: " + JSON.stringify(data));
 		
 	    return jQuery.ajax({
 	    headers: { 
 	        'Accept': 'application/json',
 	        'Content-Type': 'application/json' 
 	    },
 	   'type': 'POST' , 
 	    'url': url,
 	    'data': JSON.stringify(data),
 	    'dataType': 'json',
 	    'success': callback
 	    });
 	}

</script>


<h3>Goods</h3>
<c:if test="${!empty goodsList}">
	<table class="data">
		<tr>
			<th>ID</th>
			<th>Name</th>
			<th>Desc</th>
			<th>Price</th>
			<th>URL</th>
			<th>&nbsp;</th>
		</tr>
		<c:forEach items="${goodsList}" var="goods">
			<tr>
				<td>${goods.id}</td>
				<td>${goods.name}</td>
				<td>${goods.desc}</td>
				<td>${goods.price}</td>
				<td>${goods.url}</td>
				<td><a href="list/delete/${goods.id}">DEL</a></td>
			</tr>
		</c:forEach>
	</table>
</c:if>

</body>
</html>