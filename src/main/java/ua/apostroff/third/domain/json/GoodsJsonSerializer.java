package ua.apostroff.third.domain.json;

import java.io.IOException;

import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.JsonSerializer;
import org.codehaus.jackson.map.SerializerProvider;

import ua.apostroff.third.domain.Goods;

public class GoodsJsonSerializer extends JsonSerializer<Goods>{

	@Override
	public void serialize(Goods goods, JsonGenerator jsonGenerator,	SerializerProvider jsonProvider) throws IOException,JsonProcessingException {
		 jsonGenerator.writeStartObject();
		 jsonGenerator.writeStringField("name", goods.getName());
		 jsonGenerator.writeStringField("desc", goods.getDesc());
		 jsonGenerator.writeNumberField("price", goods.getPrice()*100);
		 jsonGenerator.writeStringField("url", goods.getUrl());
		 jsonGenerator.writeEndObject();
	}
	

}
