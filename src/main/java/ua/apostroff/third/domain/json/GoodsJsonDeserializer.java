package ua.apostroff.third.domain.json;

import java.io.IOException;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.ObjectCodec;
import org.codehaus.jackson.map.DeserializationContext;
import org.codehaus.jackson.map.JsonDeserializer;

import ua.apostroff.third.domain.Goods;

public class GoodsJsonDeserializer extends  JsonDeserializer<Goods> {
	@Override
	public Goods deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
		 ObjectCodec oc = jsonParser.getCodec();
		 JsonNode node = oc.readTree(jsonParser);
		 Goods goods = new Goods();
		 goods.setName(node.get("name").getTextValue());
		 goods.setDesc(node.get("desc").getTextValue());
		 goods.setPrice((float)node.get("price").getIntValue()/100.0f);
		 goods.setUrl(node.get("url").getTextValue());
		 return goods;
	}
}
