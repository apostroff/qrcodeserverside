package ua.apostroff.third.domain;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;

/***
 * one row in order 
 * @author apai
 *
 */



@Entity
@Table(name = "order_list")
//@JsonDeserialize(using=OrderRowJsonDeserializer.class)
public class OrderRow implements Serializable {
	private static final long serialVersionUID = 2011592784908648523L;

	@Id
	@Column(name="id" , unique = true, nullable = false, updatable = false )
	@GeneratedValue
	private Integer id;
	public Integer getId() {return id;}
	public void setId(Integer id) {	this.id = id;}
	
	
	
	@Column(name="qty")
	private Float qty;
	public Float getQty() {	return qty;}
	public void setQty(Float qty) { this.qty = qty; }
	
	
	@Column(name="discount")
	private Float discount;
	public Float getDiscount() { return discount; }
	public void setDiscount(Float discount) {this.discount = discount; }
	
	

	@ManyToOne
	@JoinColumn(name = "good_id" , insertable=false , updatable=false)
	private Goods good;
	@JsonIgnore
	public Goods getGood() {return good;}
	public void setGood(Goods good) {this.good = good;}
	

	@Column(name="good_id")
	private Integer goodid;
	public Integer getGoodid() {return goodid; }
	public void setGoodid(Integer goodid) { this.goodid = goodid; }



	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="order_id" , nullable=false )
	private Order order;
	@JsonIgnore
	public Order getOrder() {return order;}
	public void setOrder(Order order) {	this.order = order;}
	

	@Transient
	public float getSum(){
		if (getGood()==null) return 0f;
		return getQty()*getGood().getPrice()-getDiscount();
	}
	
	
	/** override object stuff */

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append(" OrderList: id=")
			.append(id)
			.append(" goods:")
			.append((good==null)?"null":good.toString())
			.append(" qty=")
			.append(qty)
			.append(" discount=")
			.append(discount);
		return buffer.toString();
	}
	
	
	
}
