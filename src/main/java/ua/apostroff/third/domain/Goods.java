package ua.apostroff.third.domain;




import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;


@SuppressWarnings("serial")
@Entity
@Table(name="Goods")
@NamedQueries({
		@NamedQuery(name = "Goods.findAll", query="select g from Goods g")
		
})
//@JsonSerialize(using = GoodsJsonSerializer.class)
//@JsonDeserialize(using = GoodsJsonDeserializer.class)

public class Goods  implements Serializable{
	@Id
	@Column(name = "id")
	@GeneratedValue
	private Integer id;
	 
	@Column
	private String name;
	 
	@Column(name="description")
	private String desc;
	 
	@Column
	private float price;
	 
	@Column
	private String url;
	
	 
	@JsonIgnore
	@OneToMany(mappedBy = "good"  )
	@Fetch(FetchMode.JOIN)
	public Set<OrderRow> orderLists = new HashSet<OrderRow>();
	
	public Integer getId() {
		return id;
	}

	public void Integer(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
	@Override
	public String toString(){
		StringBuffer buffer = new StringBuffer();
		buffer.append("id=").append(String.valueOf(id));
		buffer.append(", name=").append(name);
		buffer.append(", desc=").append(desc);
		buffer.append(", price=").append(String.valueOf(price));
		buffer.append(", url=").append(url);
		return buffer.toString();
	}


}

