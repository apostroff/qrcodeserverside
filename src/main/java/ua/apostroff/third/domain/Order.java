package ua.apostroff.third.domain;



import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;



import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;


import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(name= "order_payment")
@NamedQueries(
		@NamedQuery(name="Order.ordersWithLists" , 
		query = "select distinct o from Order o " +
						"inner join fetch o.rows r ")
		)
		
public class Order implements Serializable{
	private static final long serialVersionUID = 7623135501235539860L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column( name="id", unique = true, nullable = false, updatable = false )
	private Integer id;
	public int getId() {return id;}
	public void setId(int id) {	this.id = id;}
	
	


	
	@Temporal(TemporalType.DATE)
	@Column(name = "paymentdate")
	Date date;
	
	@Column
	String buyer;
	
	@Column 
	String paymentcard;
	
	@Column 
	String no;

	

	
	@OneToMany(fetch = FetchType.LAZY, cascade = { CascadeType.ALL,CascadeType.PERSIST,CascadeType.MERGE }, mappedBy = "order")
    @Column(nullable = false)
	private Set<OrderRow> rows = new HashSet<OrderRow>();
	public Set<OrderRow> getRows() {return rows;}
	public void setRows(Set<OrderRow> rows) {	this.rows = rows;}
	
	
	
	public String getNo() {
		return no;
	}

	public void setNo(String no) {
		this.no = no;
	}

	public float getSum(){
		float res =0;
		for(OrderRow row: rows) res+=row.getSum();
		return res;
	}
	
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getBuyer() {
		return buyer;
	}

	public void setBuyer(String buyer) {
		this.buyer = buyer;
	}

	public String getPaymentcard() {
		return paymentcard;
	}

	public void setPaymentcard(String paymentcard) {
		this.paymentcard = paymentcard;
	}
	
	@Override
	public String toString(){
		StringBuffer buffer  = new StringBuffer();
		buffer.append("Order : id=")
			.append(id)
			.append("date=")
			.append(date.toString())
			.append(" buyer")
			.append(buyer)
			.append(" card")
			.append(paymentcard)
			.append("rows: ");
		for (OrderRow row :rows)
			buffer.append(row.toString()).append(",");
		
			
		return buffer.toString();
	}

	
	
}
