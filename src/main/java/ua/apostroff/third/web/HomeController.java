package ua.apostroff.third.web;



import java.util.List;
import java.util.Map;


import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;


import ua.apostroff.third.domain.Goods;
import ua.apostroff.third.domain.Order;
import ua.apostroff.third.domain.OrderRow;
import ua.apostroff.third.service.GoodsService;
import ua.apostroff.third.service.OrderRowRepository;
import ua.apostroff.third.service.OrderService;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	@Resource(name="goodsService")
	private GoodsService goodsservice;
	@Autowired
	private OrderService orderService;
	@Autowired
	private OrderRowRepository orderRowService;
	
	@Resource(name="mailSender")
	private MailSender mailSender;
	
	public void setMailSender(MailSender mailSender) {
		this.mailSender = mailSender;
	}
	public OrderRowRepository getOrderRowService() {
		return orderRowService;
	}
	public void setOrderRowService(OrderRowRepository orderRowService) {
		this.orderRowService = orderRowService;
	}
	public OrderService getOrderService() {
		return orderService;
	}
	public void setOrderService(OrderService orderService) {
		this.orderService = orderService;
	}
	public GoodsService getGoodsservice() {
		return goodsservice;
	}
	public void setGoodsservice(GoodsService goodsservice) {
		this.goodsservice = goodsservice;
	}
	
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String getGoods(	Map<String, Object> map) {
        map.put("goods", new Goods());
        map.put("goodsList", goodsservice.findAll());
        return "goods";
	}
	
	
    @RequestMapping(value = "goodscreate", method = RequestMethod.POST)
    @ResponseBody
    public String newProductCategory(  ModelMap model, HttpServletRequest request , @RequestBody Goods goods){
    	logger.warn("post");
    	logger.warn(goods.toString());
    	return goods.toString();
    }
    
    @RequestMapping(value = "goodscreate", method = RequestMethod.GET)
    @ResponseBody
    public int gohome(){
    	return 0;
    }
    
	
	
    @RequestMapping("/list/delete/{goodsId}")
    public String deleteGoods(@PathVariable("goodsId") Integer goodsId) {
    	goodsservice.delete(goodsId);
        return "redirect:/";
    }

    @RequestMapping(value = "/list/add", method = RequestMethod.POST)
    public String addGoods(@ModelAttribute("goods") Goods goods,BindingResult result , ModelAndView model ) {
    	logger.info(model.getModel().keySet().toString());
    	goodsservice.save(goods);
        return "redirect:/";
    }
    
	
	@RequestMapping("/list")
	@ResponseBody
	public List<Goods> list(){
		logger.warn("/list "+goodsservice.findAll().size());
		return goodsservice.findAll();
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@ResponseBody
	public Goods goods(@PathVariable("id") Integer id){
		logger.warn("/goods "+id);
		return goodsservice.findById(id); 
	}
	
	@RequestMapping(value="/orders"  , method = RequestMethod.GET)
	public String getAllOrders(Map<String, Object> map){
        map.put("orders", new Order());
        map.put("orderList", orderService.findAllWithRows());
        return "orders";
	}
	@RequestMapping(value="/orders/list")
	@ResponseBody
	public List<Order> getListOrdersJson(){
		return orderService.findAllWithRows();
	}
	
	
    @RequestMapping(value = "/ordercreate", method = RequestMethod.POST)
    @ResponseBody
    public String newOrder(  ModelMap model, HttpServletRequest request , @RequestBody Order order){
    	logger.warn("post");
    	logger.warn(order.toString());
    	for (OrderRow row:order.getRows())
    		row.setOrder(order);
    	orderService.save(order);
    	sendMail(getMailMessage(order));
    	return order.toString();
    }
      
    private  void sendMail( String msg) {
    	 
		SimpleMailMessage message = new SimpleMailMessage();
 
		message.setFrom("qrcodesellerfrom@gmail.com");
		message.setTo("qrcodesellerto@gmail.com");
		message.setSubject("Noreply:Payment notification");
		message.setText(msg);
		mailSender.send(message);

    }
	
    
    private String getMailMessage(Order order){
    	StringBuffer buffer = new StringBuffer();
    	if(order!=null){
	    	buffer.append("Paid : checkout list \n")
	    		.append("Date: ").append(order.getDate()).append("\n")
	    		.append("Customer:\t").append(order.getBuyer()).append("\n")
	    		.append("Paymentcard:\t").append(order.getPaymentcard()).append("\n")
	    		.append("List : \n");
	    	float totalsum=0;
	    	for (OrderRow row: order.getRows()){
	    		Goods goods = goodsservice.findById(row.getGoodid());
	    		buffer.append(goods.getName()).append(" (").append(goods.getDesc()).append(" )\n").append("\t");
	    		buffer.append(row.getQty()).append(" x ").append(goods.getPrice()).append("=").append(row.getQty()*goods.getPrice()).append("\n");
	    		totalsum+=row.getQty()*goods.getPrice();
	    	}
	    	buffer.append("Total sum ").append(totalsum);
    	}
    	return buffer.toString();
    }
}   
        	    