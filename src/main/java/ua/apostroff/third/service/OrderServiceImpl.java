package ua.apostroff.third.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ua.apostroff.third.domain.Order;


@Service("OrderService")
@Repository
@Transactional
public class OrderServiceImpl implements OrderService {
	
	@Autowired
	OrderRepository repository;
	
	public OrderRepository getRepository() {
		return repository;
	}

	public void setRepository(OrderRepository repository) {
		this.repository = repository;
	}

	@Override
	public List<Order> findAll() {
		return (List<Order>)repository.findAll();
	}

	@Override
	public List<Order> findAllWithRows() {
		return repository.findAllWithDetails();
	}

	@Override
	public Order findById(Integer id) {
		return repository.findById(id);
	}

	@Override
	public Order save(Order order) {
		return repository.save(order);
	}

	@Override
	public void delete(Order order) {
		repository.delete(order);

	}

	@Override
	public void delete(Integer id) {
		repository.delete(id);

	}

}
