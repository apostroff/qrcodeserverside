package ua.apostroff.third.service;



import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import ua.apostroff.third.domain.Order;

public interface OrderRepository extends CrudRepository<Order, Integer> {
	Order findById(Integer id);
	
	@Query("select distinct o from Order o inner join fetch o.rows r")
	List<Order> findAllWithDetails();
}
