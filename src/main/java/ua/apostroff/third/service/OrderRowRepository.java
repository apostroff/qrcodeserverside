package ua.apostroff.third.service;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import ua.apostroff.third.domain.Goods;
import ua.apostroff.third.domain.Order;
import ua.apostroff.third.domain.OrderRow;

public interface OrderRowRepository extends CrudRepository<OrderRow, Integer> {
	List<OrderRow> findByOrder(Order order);
	List<OrderRow> findByGood(Goods goods);
	OrderRow findById(Integer id);
	
	@Query("select distinct r from OrderRow r inner join fetch r.order o")
	List<OrderRow> findAllWithDetails();
}
