package ua.apostroff.third.service;

import java.util.List;

import ua.apostroff.third.domain.Order;

public interface OrderService {
	public List<Order> findAll();
	public List<Order> findAllWithRows();
	public Order findById(Integer id);
	public Order save(Order order);
	public void delete (Order order);
	public void delete (Integer id );
}
