package ua.apostroff.third.service;

import java.util.List;

import ua.apostroff.third.domain.Goods;


public interface GoodsService {
	public List<Goods> findAll();
	
	public Goods findById(Integer id);
	public Goods save(Goods goods);
	public void delete (Goods goods);
	public void delete (Integer id );
		
}
