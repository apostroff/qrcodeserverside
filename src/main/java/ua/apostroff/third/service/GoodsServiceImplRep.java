package ua.apostroff.third.service;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ua.apostroff.third.domain.Goods;

@Service("goodsService")
@Repository
@Transactional
public class GoodsServiceImplRep implements GoodsService{
	


	@Autowired
	GoodsRepository goodsRepository;

	public GoodsRepository getGoodsRepository() {
		return goodsRepository;
	}

	public void setGoodsRepository(GoodsRepository goodsRepository) {
		this.goodsRepository = goodsRepository;
	}

	@Override
	public List<Goods> findAll() {
		return  (List<Goods>) goodsRepository.findAll();
	}



	@Override
	public Goods save(Goods goods) {
		
		return goodsRepository.save(goods);
	}

	@Override
	public void delete(Goods goods) {
		goodsRepository.delete(goods);
	}
	
	@Transactional(readOnly=true)
	public Goods findById(Integer id) {
		return goodsRepository.findById(id);
	}

	@Override
	public void delete(Integer id) {
		goodsRepository.delete( id);
		
	}
}
