package ua.apostroff.third.service;

import java.util.List;

import ua.apostroff.third.domain.Goods;
import ua.apostroff.third.domain.Order;
import ua.apostroff.third.domain.OrderRow;

public interface OrderRowService {
	List<OrderRow> findAll();
	
	OrderRow findById(Integer id);
	OrderRow save(OrderRow orderRow);
	void delete (OrderRow orderRow);
	void delete (Integer id );
	List<OrderRow> findByOrder(Order order);
	List<OrderRow> findByGood(Goods goods);
	List<OrderRow> findAllWithDetails();
	
}
