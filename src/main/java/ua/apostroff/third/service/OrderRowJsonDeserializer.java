package ua.apostroff.third.service;

import java.io.IOException;

import javax.annotation.Resource;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.ObjectCodec;
import org.codehaus.jackson.map.DeserializationContext;
import org.codehaus.jackson.map.JsonDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;


import ua.apostroff.third.domain.Goods;
import ua.apostroff.third.domain.OrderRow;

@Service
public class OrderRowJsonDeserializer extends  JsonDeserializer<OrderRow> {
	private static Logger logger  = LoggerFactory.getLogger(OrderRowJsonDeserializer.class);
	@Resource(name="goodsService")
	private GoodsService goodsService;

	@Override
	public OrderRow deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
		 ObjectCodec oc = jsonParser.getCodec();
		 JsonNode node = oc.readTree(jsonParser);
		 OrderRow row = new OrderRow(); 		 
		 if (goodsService==null){
			 logger.error("@Resource not working!!");
			 
		 }else{ 
		
			 Integer id = node.get("goodid").getIntValue();
			 if (id !=null){
				 Goods goods = goodsService.findById(id);
				 row.setGood(goods);
			 }
		 }
		 row.setQty((float) node.get("qty").asDouble());
		 row.setDiscount((float) node.get("discount").asDouble());
		 return row;
	}
	
	public GoodsService getGoodsService() {
		return goodsService;
	}
	public void setGoodsService(GoodsService goodsService) {
		this.goodsService = goodsService;
	}
}
