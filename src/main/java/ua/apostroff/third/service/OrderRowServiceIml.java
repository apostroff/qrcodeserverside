package ua.apostroff.third.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ua.apostroff.third.domain.Goods;
import ua.apostroff.third.domain.Order;
import ua.apostroff.third.domain.OrderRow;

@Service("OrderRowService")
@Repository
@Transactional
public class OrderRowServiceIml implements OrderRowService {
	@Autowired
	OrderRowRepository repository;
	
	public OrderRowRepository getRepository() {
		return repository;
	}

	public void setRepository(OrderRowRepository repository) {
		this.repository = repository;
	}

	@Override
	public List<OrderRow> findAll() {
		return  (List<OrderRow>)repository.findAll();
	}

	@Override
	public OrderRow findById(Integer id) {
		return repository.findById(id);
	}

	@Override
	public OrderRow save(OrderRow orderRow) {
		return repository.save(orderRow);
	}

	@Override
	public void delete(OrderRow orderRow) {
		repository.delete(orderRow);
	}

	@Override
	public void delete(Integer id) {
		repository.delete(id);
	}

	@Override
	public List<OrderRow> findByOrder(Order order) {
		return repository.findByOrder(order);
	}

	@Override
	public List<OrderRow> findByGood(Goods goods) {
		return repository.findByGood(goods);
	}

	@Override
	public List<OrderRow> findAllWithDetails() {
		return repository.findAllWithDetails();
	}

}
