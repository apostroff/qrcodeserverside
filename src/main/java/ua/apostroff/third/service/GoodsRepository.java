package ua.apostroff.third.service;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import ua.apostroff.third.domain.Goods;

public interface GoodsRepository extends CrudRepository<Goods, Integer> {
	public List<Goods> findByName(String name);
	public List<Goods> findByDesc(String desc);
	public Goods findById(Integer id);
}
