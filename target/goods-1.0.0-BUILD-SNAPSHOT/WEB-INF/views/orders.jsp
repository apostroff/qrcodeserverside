<%@ page language="java" contentType="text/html; charset=utf8"
	pageEncoding="utf8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf8">
	<title><spring:message code="label.title" /></title>
</head>
<body>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.13/jquery-ui.min.js"></script>

<h3><spring:message code="label.orders" /></h3>
<c:if test="${!empty orderList}">
	<table class="data" border="2">
		<tr>
			<th>id</th>
			<th><spring:message code="label.no" /></th>
			<th><spring:message code="label.paymentdate" /></th>
			<th><spring:message code="label.buyer" /></th>
			<th><spring:message code="label.paymentcard" /></th>
			<th><spring:message code="label.totalsum" /></th>
		</tr>
		<c:forEach items="${orderList}" var="order">
			<tr>
				<td>${order.id}</td>
				<td>${order.no}</td>
				<td>${order.date}</td>
				<td>${order.buyer}</td>
				<td>${order.paymentcard}</td>
				<td>${order.sum}</td>
			</tr>
			<tr>
				<th colspan="3"/>
				<th>№</th>
				<th><spring:message code="label.goods" /></th>
				<th><spring:message code="label.qty" /></th>
				<th><spring:message code="label.price" /></th>
				<th><spring:message code="label.sum" /></th>
				<th><spring:message code="label.discount" /></th>
				<th><spring:message code="label.summary" /></th>
			</tr>
			<c:forEach items="${order.rows}" var="orderrow">
				<tr >
					<td colspan="3"/>
					<td>${orderrow.id}</td>
					<td>${orderrow.good.name}</td>
					<td>${orderrow.qty}</td>
					<td>${orderrow.good.price}</td>
					<td>${orderrow.qty*orderrow.good.price}</td>
					<td>${orderrow.discount}</td>
					<td>${orderrow.qty*orderrow.good.price-orderrow.discount}</td>
				</tr>
			</c:forEach>	
		</c:forEach>
		
		
	</table>
</c:if>

<button id="testbutton" onclick="onClickTestButton();return false"> send test request</button>


<script type="text/javascript">
 	function onClickTestButton(){
 		console.debug("onClickTestButton: " );
 		var newOrder = {
 				"date":"2013-02-19",
 				"paymentcard":"new Visa 3135465844634",
 				"buyer":"apostol Andrew",
 				"rows":[{ "goodid":7,	"qty":2.0, 	"discount":1.0}],"no":"lalla"
 		};
 		
 		$.postJSON("ordercreate" , newOrder ,  function(
 			      idProductCategory)
 			     {
 			       console.debug("Inserted: " + idProductCategory);
 			     });
 			  
 	}
 	$.postJSON = function(url, data, callback) {
 		console.debug("ajax: " + JSON.stringify(data));
 		
 	    return jQuery.ajax({
 	    headers: { 
 	        'Accept': 'application/json',
 	        'Content-Type': 'application/json' 
 	    },
 	   'type': 'POST' , 
 	    'url': url,
 	    'data': JSON.stringify(data),
 	    'dataType': 'json',
 	    'success': callback
 	    });
 	}

</script>


</body>
</html>